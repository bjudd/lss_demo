--SQLed
--DDL script for Database lssdb.db

--CREATE TABLE STATEMENTS--

--sensor--
DROP TABLE IF EXISTS "sensor";
CREATE TABLE "sensor" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL , "uuid" VARCHAR(50)  NOT NULL , "name" VARCHAR(50)  NOT NULL);


--limit--
DROP TABLE IF EXISTS "limit";
CREATE TABLE "limit" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL , "uuid" VARCHAR(50)  NOT NULL , "name" VARCHAR(50)  , "type" VARCHAR(10)  , "min_val" FLOAT  , "max_val" FLOAT );


--reading--
DROP TABLE IF EXISTS "reading";
CREATE TABLE "main"."reading" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL UNIQUE , "sensor_id" INTEGER  NOT NULL , "date_time" DATETIME  NOT NULL , "location" VARCHAR(50)  NOT NULL , "b_value" FLOAT  , "h_value" FLOAT  , "t_value" FLOAT  , "b_violation" BOOL  , "h_violation" BOOL  , "t_violation" BOOL  , "b_limit_id" INTEGER  , "h_limit_id" INTEGER  , "t_limit_id" INTEGER ,FOREIGN KEY("b_limit_id") REFERENCES "limit"("id"),FOREIGN KEY("h_limit_id") REFERENCES "limit"("id"),FOREIGN KEY("t_limit_id") REFERENCES "limit"("id"));


