class ThreeLabelCell < UITableViewCell

  #
  # with help from http://blog.willrax.com/custom-uitableviewcells-in-rubymotion/
  #

  attr_accessor :headingLabel, :sideLabel, :subLabel, :subSubLabel
  attr_accessor :pauseUpdate

  def initWithStyle(style, reuseIdentifier: reuseIdentifier)
    super
    @pauseUpdate = false
    size = self.contentView.frame.size
    @headingLabel = newHeadingLabel
    @sideLabel = newSideLabel
    @subLabel = newSubLabel
    @subSubLabel = newSubSubLabel
    addLabelsToSubview
    self
  end

  def didTransitionToState(state)
    super
    NSLog "didTransitionToState: #{state.to_s}"

    @pauseUpdate = (state === 3)

    self
  end

  def addLabelsToSubview
#    NSLog "addLabelsToSubview"
    Motion::Layout.new do |layout|
      layout.view self.contentView
      layout.subviews "headingLabel" => headingLabel, "subLabel" => subLabel, "sideLabel" => sideLabel, "subSubLabel" => subSubLabel
      layout.vertical "|[headingLabel(==34)][subLabel][subSubLabel]|"
      layout.vertical "|[sideLabel]|"
      layout.horizontal "|-[headingLabel]"
      layout.horizontal "[sideLabel]-|"
      layout.horizontal "|-[subLabel]"
      layout.horizontal "|-[subSubLabel]"
    end
  end

  def newHeadingLabel
    label = UILabel.alloc.init
    label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
    label
  end

  def newSubLabel
    label = UILabel.alloc.init
    label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleFootnote)
    label
  end

  def newSubSubLabel
    label = UILabel.alloc.init
    label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleFootnote)
    label
  end

  def newSideLabel
    label = UILabel.alloc.init
#    label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)
    label.font = UIFont.boldSystemFontOfSize(36.0)
    label
  end
end
