class MainController < UIViewController
  extend IB

  attr_accessor :coolers
  attr_accessor :cooler
  attr_accessor :coolerIndex
  attr_accessor :manager
  attr_accessor :peripherals
  attr_accessor :getTemperature
  attr_accessor :getViolations
  attr_accessor :showViolationAlert
  attr_accessor :throttle

  attr_accessor :stopAlarm

  attr_accessor :dialogIsVisible

  attr_accessor :http_client


  outlet :switchOne
  outlet :switchTwo

  ib_action :switchOneAction
  ib_action :switchTwoAction
  ib_action :getInfo

  def viewDidLoad
    super
#    NSLog "MainController:viewDidLoad"

    self.navigationController.setNavigationBarHidden(false, animated:true)
    self.title = 'T-Tracks™'
    self.navigationItem.hidesBackButton = true
    self.navigationItem.rightBarButtonItem = self.editButtonItem
    self.view.backgroundColor = :gray.uicolor(0.5)

    NSLog "UIActivityIndicatorView start"
    @indicator = UIActivityIndicatorView.large
    @indicator.center = view.center
    view << @indicator
    @indicator.hidesWhenStopped = true
    @indicator.startAnimating

    5.seconds.later {
      @indicator.stopAnimating
    }

    @coolers = Array.new
    @peripherals = Array.new
    @getTemperature = false
    @getViolations = false
    @stopAlarm = false
    @showViolationAlert = true
    @dialogIsVisible = false


    @throttle = NSUserDefaults.standardUserDefaults['throttle']

    @table = UITableView.alloc.initWithFrame(CGRectMake(0,64,320,460))

    @table.autoresizingMask = UIViewAutoresizingFlexibleHeight
    self.view.addSubview(@table)

    @table.dataSource = self
    @table.delegate = self

    @manager = 0 # no CBMAnager yet

  end

  def addCooler(index)

    @coolerIndex = index

    @reader = ZBarReaderController.new
    @reader.readerDelegate = self
    @reader.scanner.setSymbology(ZBAR_I25,
                                 config: ZBAR_CFG_ENABLE,
                                 to: 0)
    self.presentModalViewController(@reader,
                                    animated: true)
  end

  def imagePickerController(reader,
                            didFinishPickingMediaWithInfo: info)
      info.objectForKey(ZBarReaderControllerResults).each do |item|
      jsonStr = BW::JSON.parse item.data.gsub("\"",'"')

      @coolers << Cooler.new(jsonStr["name"])
#      NSLog "Created new cooler object: #{jsonStr["name"]}"
      coolerIndex = @coolers.count - 1
      @cooler = @coolers[coolerIndex]
      @reader.dismissModalViewControllerAnimated(true)
#      NSLog "cooler: #{@cooler}"
#      self.tableView.setEditing(false, animated:true)
      @table.reloadData

      provisionCooler(@cooler)
    end
  end

  def provisionCooler(cooler)

    @url = NSUserDefaults.standardUserDefaults['url']
    @port = NSUserDefaults.standardUserDefaults['port']
    @user = NSUserDefaults.standardUserDefaults['user']
    @pswd = NSUserDefaults.standardUserDefaults['pswd']

    @http_client = AFMotion::Client.build_shared("http://#{@url}:#{@port}") do
      header "Accept", "application/json"
      request_serializer :json
    end

    @http_client.authorization = {username: "#{@user}", password: "#{@pswd}"}

    @http_client.get("api/v1/cooler/?name=#{cooler.name.gsub(' ','%20')}") do |result|

      if result.failure?
        # if error here, courier cannot provision cooler
        p result.error.localizedDescription
        App.alert("Internet Connetion Error: 01", {cancel_button_title: "OK", message:"#{result.error.localizedDescription}"})

      elsif result.success?
        cooler.sensor_name = result.object["objects"][0]["sensor"]

        getSensorInfo(cooler)

        start_locationManager

        @table.reloadData

        if @manager == 0
          @manager = CBCentralManager.alloc.initWithDelegate self, queue:nil
        end

        @manager.scanForPeripheralsWithServices nil, options:nil
        # if we get this far, we're off to the races...
      end
    end
  end

  def getSensorInfo(cooler)
    @http_client.get("api/v1/sensor/?name=#{cooler.sensor_name.gsub(' ','%20')}") do |result|
#    BW::HTTP.get("/api/v1/sensor/?name=#{cooler.sensor_name.gsub(' ','%20')}") do |result|
      if result.failure?
        # if error here, cannot get senrot name
        p result.error.localizedDescription
        App.alert("Internet Connetion Error: 02", {cancel_button_title: "OK", message:"#{result.error.localizedDescription}"})

      elsif result.success?
        cooler.limit_name = result.object["objects"][0]["limits"]
        getLimitInfo(cooler)
      end
    end
  end

  def getLimitInfo(cooler)
#    p "=== get limit"
    @http_client.get("/api/v1/limit/?name=#{@cooler.limit_name.gsub(' ','%20')}") do |result|
#    BW::HTTP.get("http://#{@url}:#{@port}/api/v1/limit/?name=#{@cooler.limit_name}&format=json", {credentials: {username: "#{@user}", password: "#{@pswd}"}}) do |response|
      if result.failure?
        # if error here, cannot get senrot name
        p result.error.localizedDescription
        App.alert("Internet Connetion Error: 03", {cancel_button_title: "OK", message:"#{result.error.localizedDescription}"})

      elsif result.success?
 #       p "200:limits"
        cooler.limit_max = (result.object["objects"][0]["max_value"]).to_f
        cooler.limit_min = (result.object["objects"][0]["min_value"]).to_f
        cooler.limit_name = result.object["objects"][0]["name"]
      end
    end
  end

  def start_locationManager
    if (CLLocationManager.locationServicesEnabled)
     @location_manager = CLLocationManager.alloc.init
     @location_manager.desiredAccuracy = KCLLocationAccuracyKilometer
     @location_manager.delegate = self
     @location_manager.purpose = "Our application provides functionality based on your current location"
     @location_manager.startUpdatingLocation
    else
      #App.alert('Please enable the Location Services for this app in Settings.')
#      NSLog('Please enable the Location Services for this app in Settings.')
    end
  end

  def locationManager(manager, didUpdateToLocation:newLocation, fromLocation:oldLocation)
   @cooler.latitude = newLocation.coordinate.latitude.to_s
   @cooler.longitude =  newLocation.coordinate.longitude.to_s
  end

  def locationManager(manager, didFailWithError:error)

     #App.alert('Please enable the Location Services for this app in Settings.')
#     NSLog('Please enable the Location Services for this app in Settings.')
  end

  def numberOfSectionsInTableView(tableView)
#    NSLog "numberOfSectionsInTableView"
    1
  end

  def tableView(tableView, numberOfRowsInSection:section)
#    NSLog "numberOfRowsInSection"
    #return number of rows
#    NSLog "Coolers: #{@coolers.count}"
    if self.editing?
      return @coolers.count + 1
    else
      return @coolers.count
    end
  end

  COOLER_CELL = 'Cooler Cell'

  def tableView(tableView, cellForRowAtIndexPath:indexPath)
#    NSLog "cellForRowAtIndexPath #{indexPath.section} : #{indexPath.row}"

    @reuseIdentifier ||= "cell"
    cell = tableView.dequeueReusableCellWithIdentifier(@reuseIdentifier)
    cell ||= ThreeLabelCell.alloc.initWithStyle(UITableViewCellStyleValue1, reuseIdentifier: @reuseIdentifier)

    if (@coolers.count > indexPath.row)

      # cell = tableView.dequeueReusableCellWithIdentifier(@reuseIdentifier)
      # cell ||= ThreeLabelCell.alloc.initWithStyle(UITableViewCellStyleValue1, reuseIdentifier: @reuseIdentifier)

      @cooler = @coolers[indexPath.row]
      cell.headingLabel.text = @cooler.name
      cell.subLabel.text = " "
      cell.subSubLabel.text = " "
      cell.sideLabel.text = " "

      if @cooler.connected
        cell.headingLabel.textColor = :blue.uicolor
      end

      if @cooler.provisioned

        NSLog "cellForRowAtIndexPath: #{@cooler}"

        cell.subLabel.text = @cooler.limit_name
        cell.subSubLabel.text = "#{@cooler.limit_min} min to #{@cooler.limit_max} max"
        cell.sideLabel.text = @cooler.temperature

        cell.accessoryType = UITableViewCellAccessoryNone

        cell.backgroundColor = :white.uicolor

        if @getViolations && @cooler.violation #but then...
          cell.backgroundColor = :red.uicolor
        end

      end

    else

      cell.headingLabel.text = " "
      cell.subLabel.text = " "
      cell.subSubLabel.text = " "
      cell.sideLabel.text = " "
      cell.accessoryType = UITableViewCellAccessoryNone

    end

    cell
  end

  def tableView(tableView, viewForHeaderInSection: section)
    headerView = UIView.alloc.initWithFrame(CGRectZero)
    backgroundView = UIImageView.alloc.initWithImage (UIImage.imageNamed "lss_logo_md.png")
    backgroundView.frame = [[18,-13],[300,91]]
    backgroundView.contentMode = UIViewContentModeCenter
    backgroundView.contentScaleFactor = 1
    backgroundView.userInteractionEnabled = true
    headerView.addSubview backgroundView

    label = UILabel.alloc.init
    label.frame =[[9,66],[312,15]]
    label.font = UIFont.systemFontOfSize(11.0)
    label.text = "© Copyright 2014 Lab Sensor Solutions. All rights reserved."
    label.textColor = :black.uicolor
    version = UILabel.alloc.init
    version.frame = [[107,81],[104,21]]
    version.font = UIFont.systemFontOfSize(13.0)
    version.text = "Version: #{NSBundle.mainBundle.objectForInfoDictionaryKey('appVersion')}"
    #  version.text = "Version: #{NSBundle.mainBundle.objectForInfoDictionaryKey('appVersion')}"
    version.textColor = :black.uicolor

    barView = UIView.alloc.initWithFrame(CGRectZero)
    barView.frame = [[0,105],[320,1]]
    barView.backgroundColor = :black.uicolor

    headerView.addSubview(label)
    headerView.addSubview(version)
    headerView.addSubview(barView)
    tableView.tableHeaderView = headerView

    return headerView
  end

  def tableView(tableView, heightForHeaderInSection: section)
    110.0
  end

  def tableView(tableView, heightForRowAtIndexPath:indexPath)
    75.0
  end

  # def tableView(tableView, didSelectRowAtIndexPath:indexPath)
  #   cell = tableView.cellForRowAtIndexPath(indexPath)
  #   if (cell.textLabel.text == "")
  #     addCooler(indexPath.section)
  #   else
  #     NSLog "didSelectRowAtIndexPath: #{@coolers[indexPath.row].name}"
  #     self.performSegueWithIdentifier('ShowDetail', sender:@coolers[indexPath.row])
  #   end
  # end

  def setEditing(isEditing, animated:animated)
    NSLog "setEditing"

    super(isEditing, animated:animated)

    last_index_path = [NSIndexPath.indexPathForRow(@coolers.count, inSection:0)]

#    self.tableView.setEditing(isEditing, animated:animated)
    @table.setEditing(isEditing, animated:animated)


    if (isEditing)
#      tableView.insertRowsAtIndexPaths(last_index_path, withRowAnimation:UITableViewRowAnimationBottom)
      @table.insertRowsAtIndexPaths(last_index_path, withRowAnimation:UITableViewRowAnimationBottom)
    else
#      tableView.deleteRowsAtIndexPaths(last_index_path, withRowAnimation:UITableViewRowAnimationBottom)
      NSLog "deleteRowsAtIndexPaths"
      @table.deleteRowsAtIndexPaths(last_index_path, withRowAnimation:UITableViewRowAnimationBottom)
    end

  end

  def tableView(tableView, commitEditingStyle: editing, forRowAtIndexPath: indexPath)

    if( editing == UITableViewCellEditingStyleDelete )
      NSLog "UITableViewCellEditingStyleDelete"
      cell = tableView.cellForRowAtIndexPath(indexPath)
      cell.detailTextLabel.text = ""
      cell.textLabel.text = ""
      @coolers.delete_at(indexPath.row)
      tableView.reloadData
     # @table.reloadData

    else
      addCooler(indexPath.section)
    end

  end

  def tableView(tableView, editingStyleForRowAtIndexPath: indexPath)

    if (indexPath.row == @coolers.count)
        return UITableViewCellEditingStyleInsert
               #gives green circle with +
    else
      NSLog "editingStyleForRowAtIndexPath"

        return UITableViewCellEditingStyleDelete
               #or UITableViewCellEditingStyleNone
    end
  end


# CBCentralManager delegate
  def centralManagerDidUpdateState(state)
#    NSLog "centralManagerDidUpdateState"
    state = nil;

    case @manager.state
    when CBCentralManagerStateUnsupported
      state = "The platform/hardware doesn't support BLE"
    when CBCentralManagerStateUnauthorized
      state = "The app is not authorized to use BLE"
    when CBCentralManagerStatePoweredOff
      state = "BLE is currently powered off"
    else
      NSLog "scanForPeripheralsWithServices"
      manager.scanForPeripheralsWithServices nil, options:nil
      return false
    end

#    NSLog "Central manager state: %@", state

    alert = UIAlertView.alloc.initWithTitle "Bluetooth status",
                              message: state,
                              delegate: nil,
                              cancelButtonTitle: "Dismiss",
                              otherButtonTitles: nil
    alert.show

    return false
  end

  def centralManager(manager, didDiscoverPeripheral:peripheral, advertisementData:data, RSSI:rssi)
#    NSLog("found #{peripheral.name} - cooler: #{@cooler.name} sensor: #{@cooler.sensor_name}")

    if (@cooler.sensor_name != peripheral.name)
#      NSLog "peripheral: #{peripheral.name} != cooler: #{@cooler.name} sensor: #{@cooler.sensor_name}"
      return
    end

    unless peripherals.containsObject(peripheral)
      peripherals << peripheral
    end

    @cooler.connected = true
    @table.reloadData

    manager.connectPeripheral peripheral, options:nil
  end

  def centralManager(central, didConnectPeripheral:peripheral)
#    NSLog "connected to #{peripheral.name}"

    @cooler.peripheral = peripheral
#    NSLog "didConnectPeripheral -- cooler: #{@cooler} peripheral: #{@cooler.peripheral.name}"

    @cooler.sensor_name = peripheral.name

    peripheral.delegate = self

    peripheral.discoverServices nil

  end

  def centralManager(central, didFailToConnectPeripheral:peripheral, error:error)
#    NSLog "Failed to connect to peripheral: %@", peripheral.name

  end

  #CBPeripheral delegate

  IR_TEMP_SERVICE_UUID = "f000aa00-0451-4000-b000-000000000000"
  IR_TEMP_DATA_UUID = "f000aa01-0451-4000-b000-000000000000"
  IR_TEMP_CONFIG_UUID = "f000aa02-0451-4000-b000-000000000000"

  def peripheral(peripheral, didDiscoverServices:error)
#    NSLog "main:didDiscoverServices: %@", error

    found = false
    replace = false

    #@manager.cancelPeripheralConnection peripheral
    #@manager.stopScan

    return if error || !peripheral.services

      peripheral.services.each do |service|

 #       NSLog "Service found with UUID: %@", service.UUID

        if service.UUID == CBUUID.UUIDWithString(IR_TEMP_SERVICE_UUID)
  #        NSLog "-> Found IR_TEMP_SERVICE_UUID"
          peripheral.discoverCharacteristics(nil,forService:service)
        end

        if service.UUID == CBUUID.UUIDWithString("180a")
  #        NSLog "-> Found service device info"
          peripheral.discoverCharacteristics nil, forService:service
  #        NSLog "-> Found service device info 2"
        end
      end
  end

  def peripheral(peripheral, didDiscoverCharacteristicsForService:service, error:error)

 #   NSLog "main:didDiscoverCharacteristicsForService: %@ error %@", service, error

    if service.UUID == CBUUID.UUIDWithString("180a")
      service.characteristics.each do |char|
        if char.UUID == CBUUID.UUIDWithString("2a29")
  #        NSLog "Found Device Manufacturer Name"
          peripheral.readValueForCharacteristic char
        end
        if char.UUID == CBUUID.UUIDWithString("2a00")
  #        NSLog "Found Device Name"
          peripheral.readValueForCharacteristic char
        end
      end
    end

    if service.UUID == CBUUID.UUIDWithString(IR_TEMP_SERVICE_UUID)

#      NSLog "-->activateAmbientTemperature"

      sUUID = CBUUID.UUIDWithString IR_TEMP_SERVICE_UUID
      cUUID = CBUUID.UUIDWithString IR_TEMP_CONFIG_UUID

      data_ptr = Pointer.new(:uchar)
      data_ptr[0] = 0x1

#      NSLog "-->writeCharacteristic"
      BLEUtility.writeCharacteristic(peripheral, sUUID:sUUID, cUUID:cUUID, data:(NSData.dataWithBytes(data_ptr, length:1)))

      cUUID = CBUUID.UUIDWithString IR_TEMP_DATA_UUID

#      NSLog "-->setNotificationForCharacteristic"
      BLEUtility.setNotificationForCharacteristic (peripheral, sUUID:sUUID, cUUID:cUUID, enable:true)
    end
  end

  def peripheral(peripheral, didUpdateValueForCharacteristic:characteristic, error:error)
#    NSLog "main:peripheral: didUpdateValueForCharacteristic"
    # if (peripheral.name != @cooler.sensor_name)
    #   NSLog "peripheral: #{peripheral.name} != cooler: #{@cooler.name} sensor: #{@cooler.sensor_name}"
    #   return
    # end
    #NSLog "didUpdateValueForCharacteristic: #{peripheral.name}"
    @coolers.each do |c|
      if c.sensor_name == peripheral.name
        @cooler = c
        break
      end
    end

#    NSLog "main:peripheral: #{peripheral.name} == cooler: #{@cooler.name} sensor: #{@cooler.sensor_name}"

    case characteristic.UUID
    when CBUUID.UUIDWithString("2a29")
#      NSLog "--> read device mfr name #{characteristic.value}"

    when CBUUID.UUIDWithString(IR_TEMP_DATA_UUID)
#      NSLog "--> read SensorTMP006"
      tAmb = SensorTMP006::calcTAmb (characteristic.value)
      @cooler.temperature = sprintf("%.2f°C",tAmb)
#      NSLog "----> Ambient Temperature = #{@cooler.temperature}"

      @cooler.provisioned = true
#      self.tableView.reloadData
      if @getTemperature

        @table.reloadData unless self.editing?

#        NSLog "throttle_count: #{@cooler.throttle_count}"
        if @cooler.throttle_count.to_i == 0
          postReadingToCBAE (@cooler,tAmb)

          if @getViolations # if we are checking for violations...
            @cooler.violation = false
            if (tAmb > @cooler.limit_max.to_i)
              postViolationToCBAE(@cooler,tAmb,"max",@cooler.limit_name)
             # if @stopAlarm == false
 #             NSLog "playAlarm"
              playAlarm
              # end
#              NSLog "alarm"
              # setup_alarm_notification(@cooler)
              # if @showViolationAlert == true
              @cooler.violation = true
        #      violationAlert (@cooler)
            # end
            end
            if (tAmb < @cooler.limit_min.to_i)
              postViolationToCBAE(@cooler,tAmb,"min",@cooler.limit_name)

              # if @stopAlarm == false
#              NSLog "playAlarm"
              playAlarm
              # end
 #             NSLog "alarm"
              # setup_alarm_notification(@cooler)
              # if @showViolationAlert == true
              @cooler.violation = true
         #     violationAlert (@cooler)
              # end
            end

          end
        end

        @cooler.throttle_count = @cooler.throttle_count + 1

        if @cooler.throttle_count.to_i > @throttle.to_i
          @cooler.throttle_count = 0
        end

      end
    end
  end

  def violationAlert (cooler)
    @alert ||= UIAlertView.alloc.initWithTitle("Compliance Violation", message: "Cooler #{cooler.name} is out of compliance", delegate: self, cancelButtonTitle: "Acknowledge", otherButtonTitles: nil)
    @showViolationAlert = false
    @stopAlarm = true
    @alert.show
  end

  def postReadingToCBAE(cooler,temperature)
    NSLog "Reading: #{cooler.name}, #{temperature}"
    @url = NSUserDefaults.standardUserDefaults['url']
    @port = NSUserDefaults.standardUserDefaults['port']
    @user = NSUserDefaults.standardUserDefaults['user']
    @pswd = NSUserDefaults.standardUserDefaults['pswd']

    @http_client.post("/api/v1/read/",
                name: cooler.name,
                type: "Cooler",
                temperature: temperature,
                lat: cooler.latitude,
                lon: cooler.longitude,
                datetime: Time.now.to_s,
                assigned: @user ) do |result|
#   BW::HTTP.post("http://#{@url}:#{@port}/api/v1/read/?format=json", {headers: {"Content-Type" => "application/json"} ,payload: data, credentials: {username: "#{@user}", password: "#{@pswd}"}}) do |response|
      if result.failure?
      # TO DO: here's where posting to onboard sqlite db occurs
      end

    end
  end

  def postViolationToCBAE(cooler, temperature, type, limit_name)
    NSLog "Violation: #{cooler.name}, #{temperature}, #{type}, #{limit_name}"
    @url = NSUserDefaults.standardUserDefaults['url']
    @port = NSUserDefaults.standardUserDefaults['port']
    @user = NSUserDefaults.standardUserDefaults['user']
    @pswd = NSUserDefaults.standardUserDefaults['pswd']

#    NSLog "POST violation: #{data}"
    @http_client.post("/api/v1/viol/",
                name: cooler.name,
                type: "Cooler",
                temperature: temperature,
                limit_name: limit_name,
                lat: cooler.latitude,
                lon: cooler.longitude,
                datetime: Time.now.to_s,
                assigned: @user ) do |result|
#
 #   BW::HTTP.post("http://#{@url}:#{@port}/api/v1/viol/?format=json", {headers: {"Content-Type" => "application/json"}, payload: data, credentials: {username: "#{@user}", password: "#{@pswd}"}}) do |response|
      if result.failure?
          # TO DO: here's where posting violations to onboard sqlite db occurs
      end
    end
  end

  def local_file
    NSURL.fileURLWithPath(File.join(NSBundle.mainBundle.resourcePath, 'short_alarm.mp3'))
  end

  def playAlarm
    if @getViolations
#      NSLog "playAlarm"
      Dispatch::Queue.concurrent.async do
        mainBundle = NSBundle.mainBundle
        filePath = mainBundle.pathForResource("short_alarm", ofType:"mp3")
        fileData = NSData.dataWithContentsOfFile(filePath)
        error = Pointer.new(:object)

        @audio_player = AVAudioPlayer.alloc.initWithContentsOfURL(local_file, error:error)

        @audio_player.delegate = self
        if (@audio_player.prepareToPlay && @audio_player.play)
#          p "Successfully playing"
        else
#          p "Failed to Play"
        end
      end
    end
  end

  def switchOneAction sender
#    NSLog "switchOneAction"
    @getTemperature = !@getTemperature
  end

  def switchTwoAction sender
#    NSLog "switchTwoAction"
    @getViolations = !@getViolations
    @table.reloadData
  end

  def setup_alarm_notification(cooler)
#    NSLog "setup_alarm_notification"
    # alarm_date = App::Persistence[for_day]
    # alarm_is_set = App::Persistence[with_switch];

    # if (alarm_is_set && alarm_date && (alarm_date > Time.now))
        local_notification = UILocalNotification.alloc.init
        local_notification.fireDate = NSDate.dateWithString( Time.now.to_s )
        local_notification.alertBody = "#{cooler.name} is out of compliance"
        local_notification.alertAction = "Confirm"
        local_notification.soundName = UILocalNotificationDefaultSoundName
        local_notification.applicationIconBadgeNumber = 1

#        local_notification.userInfo = {MONDAY_ALARM_KEY => alarm_date}

#        UIApplication.sharedApplication.scheduleLocalNotification(local_notification)
        UIApplication.sharedApplication.presentLocalNotificationNow(local_notification)
        NSLog("alarm notification set for: #{Time.now}")
      # end
  end

  def network_alert(msg)
    # NSLog "network_alert: dialogIsVisible = #{@dialogIsVisible}"
    # if @dialogIsVisible == false
    #   @dialogIsVisible == true
    #   BW::UIAlertView.new({
    #     title: 'CBAE Connection Error',
    #     message: "#{msg}",
    #     buttons: ['Cancel'],
    #     cancel_button_index: 0
    #   }) do |alert|
    #     if alert.clicked_button.cancel?
    #       @dialogIsVisible == false
    #     end
    #   end.show
    # end
  end

  def getInfo
  end
end
