include SugarCube::Adjust

class AppDelegate

  attr_accessor :url, :user, :pswd, :test

  def application(application, didFinishLaunchingWithOptions:launchOptions)

    setDefaultSettings

    @window = UIWindow.alloc.initWithFrame UIScreen.mainScreen.bounds
    storyboard = UIStoryboard.storyboardWithName "Storyboard", bundle: nil
    @window.rootViewController = storyboard.instantiateInitialViewController
    @window.makeKeyAndVisible

    #NSLog "Version: #{NSBundle.mainBundle.objectForInfoDictionaryKey('CFBundleShortVersionString')}"

    true
  end

  def didReceiveLocalNotification(notifocation)
#    p "didReceiveLocalNotification"
  end

  def applicationWillBecomeActive(application)
#    p "applicationWillBecomeActive"
  end

  def applicationWillEnterForeground(application)
#    p "applicationWillEnterForeground"
  end

  def setDefaultSettings
#    NSLog "setDefaultSettings"
    textValue = NSUserDefaults.standardUserDefaults.stringForKey "cat"

    # If the first value is nil, then we know that the defaults are not set.
    if(textValue == nil)
      bPath = NSBundle.mainBundle.bundlePath
      settingsPath = bPath.stringByAppendingPathComponent "Settings.bundle"
      plistFile    = settingsPath.stringByAppendingPathComponent "Root.plist"

      settingsDictionary = NSDictionary.dictionaryWithContentsOfFile plistFile
      preferencesArray   = settingsDictionary.objectForKey "PreferenceSpecifiers"

      appPrerfs = {}

      preferencesArray.each do |item|
        # Get the key of the item.
        keyValue = item.objectForKey "Key"

        # Get the default value specified in the plist file.
        defaultValue = item.objectForKey "DefaultValue"

        appPrerfs[keyValue] = defaultValue unless keyValue.nil? || defaultValue.nil?
      end

      NSUserDefaults.standardUserDefaults.registerDefaults appPrerfs
      NSUserDefaults.standardUserDefaults.synchronize

    end
  end

end
