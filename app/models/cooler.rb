class Cooler

  attr_accessor :id
  attr_accessor :name
  attr_accessor :uuid
  attr_accessor :sensor_name
  attr_accessor :limit_name
  attr_accessor :limit_min
  attr_accessor :limit_max
  attr_accessor :checkLimits
  attr_accessor :provisioned
  attr_accessor :peripheral
  attr_accessor :temperature
  attr_accessor :longitude
  attr_accessor :latitude
  attr_accessor :violation
  attr_accessor :connected # if Cooler/sensor peripheral is discovered, set text color to green
  attr_accessor :throttle_count

  def initialize(name)
    @name = name
    @provisioned = false
    @checkLimits = false
    @peripheral = nil
    @violation = false
    @connected = false
    @throttle_count = 0
    @pauseUpdate = false
  end

end
