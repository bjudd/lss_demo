class BLEUtility

=begin

converted from ...

//
//  BLEUtility.m
//
//  Created by Ole Andreas Torvmark on 9/22/12.
//  Copyright (c) 2012 Texas Instruments All rights reserved.
//

#import "BLEUtility.h"

=end

  def self.writeCharacteristic (peripheral, sUUID:sUUID, cUUID:cUUID, data:data)
    NSLog "writeCharacteristic %@ %@ %@ %@", peripheral, sUUID, cUUID, data
    peripheral.services.each do |service|
      if service.UUID == sUUID
        service.characteristics.each do |characteristic|
          if characteristic.UUID == cUUID
        #    value = Pointer.new(:uchar,8)
        #    value[0] = data
        #    valData = NSData.dataWithBytes(value[0], length:8)
            peripheral.writeValue (data, forCharacteristic:characteristic, type:CBCharacteristicWriteWithResponse)
            NSLog "writeCharacteristic complete"
          end
        end
      end
    end

  end

  def self.readCharacteristic (peripheral, sUUID:sUUID, cUUID:cUUID)
    NSLog "readCharacteristic %@ %@ %@", peripheral, sUUID, cUUID
    peripheral.services.each do |service|
      if service.UUID == sUUID
        service.characteristics.each do |characteristic|
          if characteristic.UUID == cUUID
            peripheral.readValueForCharacteristic(characteristic)
          end
        end
      end
    end
  end

  def self.setNotificationForCharacteristic (peripheral, sUUID:sUUID, cUUID:cUUID, enable:enable)
    NSLog "setNotificationForCharacteristic %@ %@ %@ %@", peripheral, sUUID, cUUID, enable
    peripheral.services.each do |service|
      if service.UUID == sUUID
        service.characteristics.each do |characteristic|
          if characteristic.UUID == cUUID
             peripheral.setNotifyValue(enable, forCharacteristic:characteristic)
          end
        end
      end
    end
  end

end #class
